package com.cyan.bubble;

public class Constant {
	public static final int iteration = 1;
	public static final int listSize =5;
	public static final int listNumber = 100000;
	public static int total = 0;
	public static final double P = .5;
	public static int case1 = 0;
	public static int case2 = 0;

	public static void reliability() {
		int expected = Constant.total / Constant.iteration;
		System.out.println("Expected number of correctly sorted list: "
				+ expected);
		System.out.println("Expected number of correctly sorted list where no incorrect comparison occured: "
				+ case1/Constant.iteration);
		System.out.println("Expected number of correctly sorted list where incorrect comparison occured:  "
				+ case2/Constant.iteration);
		double reliabile = 100 - (1.0 * (Constant.listNumber - expected))
				/ (1.0 * Constant.listNumber) * 100;
		System.out.println("Reliability: " + reliabile + "%");
	}
}
