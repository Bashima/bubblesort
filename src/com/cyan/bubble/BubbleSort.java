package com.cyan.bubble;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Random;

public class BubbleSort {

	public ArrayList<CustomList> listArray;

	public BubbleSort() {
		listArray = new ArrayList<CustomList>();
	}

	public void swap(int index, int a, int b) {
		int temp = listArray.get(index).list.get(a);
		listArray.get(index).list.set(a, listArray.get(index).list.get(b));
		listArray.get(index).list.set(b, temp);
	}

	public void sort(CustomList listItem, int index) {
		Compare comparator = new Compare();

		int n = listItem.list.size();

		while (true) {
			boolean swapped = false;
			for (int i = 1; i < n; i++) {

				if (comparator.unreliableComparison(listItem.list.get(i - 1),
						listItem.list.get(i))) {
					
				} else {
					swap(index, i - 1, i);
					swapped = true;
				}
				if (listItem.list.get(i - 1) > listItem.list.get(i)) {
					listItem.wrongComparison = true;
				}
				
			}

			if (swapped == false) {
//				System.out.println("Output List: " + listItem.list.toString());
				break;
			}

		}
		for (int i = 1; i < n; i++) {
			if (listItem.list.get(i - 1) > listItem.list.get(i)) {
				listItem.correct = false;
				break;
			}
		}
	}

	public void correctCount() {
		int case1Correct = 0;
		int case2Correct = 0;
		int totalCorrect = 0;
		for (CustomList listItem : listArray) {
			if (listItem.correct) {
				totalCorrect++;
			}
			if (listItem.correct && listItem.wrongComparison) {
				case2Correct++;
			} 
			if (listItem.correct && !listItem.wrongComparison) {
				case1Correct++;
			}
		}
		System.out.println("Number of correctly sorted list: " + totalCorrect);
		System.out.println("Case1: "+ case1Correct + ", Case 2: "+ case2Correct);
		Constant.total = Constant.total + totalCorrect;
		Constant.case1 = Constant.case1 + case1Correct;
		Constant.case2 = Constant.case2 + case2Correct;
	}

	public static void main(String[] args) throws FileNotFoundException {
//		PrintStream out = new PrintStream(new FileOutputStream("output.txt"));
//		System.setOut(out);
		for (int k = 0; k < Constant.iteration; k++) {
			BubbleSort bubble = new BubbleSort();
			for (int i = 0; i < Constant.listNumber; i++) {
				CustomList tempList = new CustomList();
				while (tempList.list.size() < Constant.listSize) {
					Random rand = new Random();
					int value = rand.nextInt(Constant.listSize*3);
					tempList.list.add(value);
				}
				bubble.listArray.add(tempList);
			}
			for (CustomList listItem : bubble.listArray) {
				System.out.println("Input List: " + listItem.list.toString());
				int index = bubble.listArray.indexOf(listItem);
				bubble.sort(listItem, index);
			}
			bubble.correctCount();
		}
//		Constant.reliability();
	}

}
