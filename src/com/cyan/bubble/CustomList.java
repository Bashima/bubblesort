package com.cyan.bubble;

import java.util.ArrayList;

public class CustomList {
	public ArrayList<Integer> list;
	public boolean correct;
	public boolean wrongComparison;

	public CustomList() {
		list = new ArrayList<Integer>();
		correct = true;
		wrongComparison = false;
	}

}
