package com.cyan.bubble;

import java.util.Random;

public class Compare {
	
	public Random rand;
	
	public Compare()
	{
		rand = new Random();
	}

	public boolean reliableComparison(int a, int b) {
		if (a < b) {
			return true;
		} else {
			return false;
		}
	}

	public boolean reliableComparisonEqual(int a, int b) {
		if (a <= b) {
			return true;
		} else {
			return false;
		}
	}

	public boolean unreliableComparison(int a, int b) {
		rand = new Random();
		double value = rand.nextDouble();
//		 System.out.println(value);
		if (a <= b) {
			if (value < Constant.P) {
				return true;
			}
//			System.out.println("wrong");
			return false;
		} else {
			if (value < Constant.P) {
				return false;
			}
//			System.out.println("wrong");
			return true;
		}
	}
}
